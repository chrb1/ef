# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 16:04:02 2017

@author: christophe
"""

import numpy as np
import scipy as sp
import scipy.linalg as la

def func(k1=-4e6, k2=1., t=1.0e-9, eps=1.0e-6):
    return np.array([[k1, k2, k2],
                     [k2,  t, 0.],
                     [eps, 0.,0.]])


a = func()
b = np.array([1.0, 1., 1.])
lu,piv = sp.linalg.lu_factor(a)
x = la.lu_solve((lu,piv),b)
print (x)
print (np.dot(a,x) - b)
