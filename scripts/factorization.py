# -*- coding: utf-8 -*-
"""
Version du 10 Novembre 2017
Differentes factorisations classiques en algèbre linéaire dense.
Algos issus du livre de Alfio.
Réalisé pour le cours EF des Mines.
"""

from math import sqrt
import numpy as np
import scipy as sp
import scipy.sparse as spa
import scipy.sparse.linalg as sla
import scipy.linalg as la
import pylab as plt

#plt.style.use("live")

from itertools import cycle
markers = cycle("osx+")

#TODO: Enlever la contrainte sur la taille du RHS

def forward_row(L, b):
    """
    Substitution directe (version orientée ligne) (Forward substitution)

    forward_row résout le syst. tri. inf. L x = b
    avec la méthode de substitution directe dans sa version orientée ligne

    L est une matrice carré dense d'ordre n, b est le rhs
    """
    n, m = L.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")
    if(b.size != n):
        raise ValueError("Bad rhs size")
    if (abs(L.diagonal())).min() < 1.0e-20:
        raise ArithmeticError("Singular system")

    x = np.zeros(n)
    x[0] = b[0] / L[0,0]
    for i in range(1,n):
        x[i] = (b[i] - np.dot(L[i,:i], x[:i])) / L[i,i]

    return x


def forward_col(L, b):
    """
    Substitution directe (version orientée colonne) (Forward substitution)

    forward_col résout le syst. tri. inf. L x = b
    avec la méthode de substitution directe dans sa version orientée colonne

    L est une matrice carré dense d'ordre n, b est le rhs
    Attention, ici b est écrasé par la solution
    """
    n, m = L.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")
    if(b.size != n):
        raise ValueError("Bad rhs size")
    if (abs(L.diagonal())).min() < 1.0e-20:
        raise ArithmeticError("Singular system")

    for j in range(n-1):
        b[j] /= L[j,j]
        b[j+1:] -= (b[j] * L[j+1:,j])
    b[n-1] /= L[n-1,n-1]

    return b


def backward_col(U, b):
    """
    Substitution retrograde (version orientée colonne) (Backward substitution)

    backward_col résout le syst. tri. sup. U x = b
    avec la méthode de substitution retrograde dans sa version orientée colonne

    U est une matrice carré dense d'ordre n, b est le rhs
    Attention, ici b est écrasé par la solution
    """
    n, m = U.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")
    if(b.size != n):
        raise ValueError("Bad rhs size")
    if (abs(U.diagonal())).min() < 1.0e-20:
        raise ArithmeticError("Singular system")

    for j in range(n-1, 0, -1):
        b[j] /= U[j,j]
        b[:j] -= (b[j] * U[:j,j])
    b[0] /= U[0,0]

    return b


def backward_row(U, b):
    """ """
    raise NotImplementedError("TODO")


def lu_kji(A):
    """
    Factorisation LU (version kji) la plus simple possible.
    Il n'y a pas de changement de pivot.
    Attention, les modifications sont inplace, A est écrasée
    U est stockée dans la partie tri. sup. de A.
    L est stockée dans la partie tri. inf. strict. de A

    Aussi appelée SAXPY car l'opération de base est
    Scalar A  times VECTOR X plus Y, cf line :
        A[k+1:,j] -= A[k+1:, k] * A[k,j]
        Y             X           A

    """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")

    for k in range(0, n-1):
        if abs(A[k,k]) < 1.0e-20:
            raise ArithmeticError("Null pivot at step %d" %k)
        # i = k+1:n
        A[k+1:,k] /= A[k,k]
        for j in range(k+1,n):
            A[k+1:,j] -= A[k+1:, k] * A[k,j]
    return A


def lu_jki(A):
    """
    Factorisation LU (version jki) la plus simple possible.
    Il n'y a pas de changement de pivot.
    Attention, les modifications sont inplace, A est écrasée
    U est stockée dans la partie tri. sup. de A.
    L est stockée dans la partie tri. inf. strict. de A

    Aussi appelée GAXPY car l'opération de base est
    TODO:

    """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")

    for j in range(0,n):
        if abs(A[j,j]) < 1.0e-20:
            raise ArithmeticError("Null pivot at step %d" %j)
        for k in range(0, j):
            A[k+1:, j] -= A[k+1:,k] * A[k,j] # i = k+1:n
        A[j+1:,j] /= A[j,j]
    return A


def lu_ijk(A):
    """
    Factorisation LU (version ijk) la plus simple possible.
    Il n'y a pas de changement de pivot.
    Attention, les modifications sont inplace, A est écrasée
    U est stockée dans la partie tri. sup. de A.
    L est stockée dans la partie tri. inf. strict. de A

    """
    raise NotImplementedError("TODO")


def cholesky(A):
    """
    Factorisation de Cholesky de la matrice A
    Seulement valide pour les matrices symétriques définies positives
    Il n'y a pas de changement de pivot (car ce n'est pas nécessaire)
    Attention, les modifications sont inplace, A est écrasée
    Retourne une matrice L tri. inf. telle que  np.dot(L,L.transpose()) = A
    """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")

    for k in range(n-1):
        if A[k,k] < 1.0e-20:
            raise ArithmeticError("Null pivot at step %d, \n Are you SPD ?" %k)
        A[k,k] = sqrt(A[k,k])
        A[k+1:,k] /= A[k,k]
        for j in range(k+1, n):
            # i = k+1:n
            A[j:, j] -= A[j:,k] * A[j,k]
    A[n-1,n-1] = sqrt(A[n-1,n-1])
    A = np.tril(A)
    return A

def partial_pivot(A, k):
    """ """
    # find the best pivot
    n = A.shape[0]
    piv = np.argmax(abs(A[k:,k]))
    ipiv = k+piv
    Pk = np.eye(n)
    Pk[k,k] = 0
    Pk[ipiv,ipiv] = 0
    Pk[k,ipiv] = 1.
    Pk[ipiv,k] = 1.
    return Pk


def gauss_step(A, k):
    """ """
    Mk = np.eye(A.shape[0])
    Mk[k+1:, k] = - A[k+1:, k] / A[k,k]
    Mkinv = 2 * np.eye(A.shape[0]) - Mk
    return Mk, Mkinv

def lu_kji_partial(A):
    """
    Factorisation LU (version kji) avec pivot partiel
    Attention, A est écrasée
    Retourne L,U,P telle que PA = LU
    TODO: version compacte et optimisée
    """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")
    P, Minv = np.eye(n),np.eye(n)
    for k in range(0, n-1):
        Pk = partial_pivot(A, k)
        A = sp.dot(Pk, A)
        Mk, Mkinv = gauss_step(A, k)
        A = sp.dot(Mk, A)
        P = sp.dot(Pk, P)
        Minv = sp.dot(Minv, Pk)
        Minv = sp.dot(Minv, Mkinv)
    U = sp.triu(A)
    L = sp.dot(P, Minv)
    return L, U, P

def total_pivot(A, k):
    """ """
    # find the best pivot
    n = A.shape[0]
    B = np.abs(A[k:,k:])
    pivi, pivj = np.unravel_index(B.argmax(), B.shape)
    ipiv, jpiv = k+pivi, k+pivj
    Pk, Qk = np.eye(n), np.eye(n)
    Pk[k,k] = 0
    Pk[ipiv,ipiv] = 0
    Pk[k,ipiv] = 1.
    Pk[ipiv,k] = 1.
    Qk[k,k] = 0
    Qk[jpiv,jpiv] = 0
    Qk[k,jpiv] = 1.
    Qk[jpiv,k] = 1.
    return Pk, Qk

def lu_kji_total(A):
    """
    Factorisation LU (version kji) avec pivot total
    Attention, A est écrasée
    Retourne L,U,P,Q telle que PAQ = LU
    TODO: version compacte et optimisée
    """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")

    P, Q, Minv = np.eye(n), np.eye(n), np.eye(n)
    for k in range(0, n-1):
        Pk, Qk = total_pivot(A, k)
        A = sp.dot(sp.dot(Pk, A), Qk)
        Mk, Mkinv = gauss_step(A, k)
        A = sp.dot(Mk, A)
        P = sp.dot(Pk, P)
        Q = sp.dot(Q, Qk)
        Minv = sp.dot(sp.dot(Minv, Pk), Mkinv)
    U = sp.triu(A)
    L = sp.dot(P, Minv)
    return L, U, P, Q

def _func_pivot_test(k=1e-8):
    return np.array([[k , 1., 0.],
                     [10., 1., 0.],
                     [0., 0., 1.]])

#def _func_pivot_test(k=1e-8):
#    return np.array([[k , 1000., 1000.],
#                     [10.*k, 1., 0.],
#                     [0., 0., 1.]])
#

def _test_pivot(k=1e-10, gene=_func_pivot_test, lu=lu_kji):
    """ """
    A = gene(k=k)
    x_ex = np.array([1., 1., 1.])
    b = A.dot(x_ex)
    LU = lu(A.copy())
    L = np.tril(LU)
    L[np.diag_indices(A.shape[0])] = 1.
    U = np.triu(LU)
    y = b.copy()
    forward_col(L,y)
    backward_col(U,y)
    return y, la.norm(x_ex-y)

def _test_pivot_partial(k=1e-10, gene=_func_pivot_test):
    """ """
    A = gene(k=k)
    x_ex = np.array([1., 1., 1.])
    b = A.dot(x_ex)
    L, U, P = lu_kji_partial(A.copy())
    y = np.dot(P, b)
    forward_col(L,y)
    backward_col(U,y)
    return y, la.norm(x_ex-y)

def _test_pivot_total(k=1e-10, gene=_func_pivot_test):
    """ """
    A = gene(k=k)
    x_ex = np.array([1., 1., 1.])
    b = A.dot(x_ex)
    L, U, P, Q = lu_kji_total(A.copy())
    y = np.dot(P, b)
    forward_col(L,y)
    backward_col(U,y)
    y = np.dot(Q, y)
    return y, la.norm(x_ex-y)

def pivot_example():
    """
    Petit exemple pour montrer le caractère néfaste des
    petits pivots sur la précision du calcul
    """
    without_pivot = list()
    with_partial_pivot = list()
    with_total_pivot = list()
    pivots = np.logspace(-10,-1,num=10)
    for k in pivots:
        without_pivot.append(_test_pivot(k)[1])
        with_partial_pivot.append(_test_pivot_partial(k)[1])
        with_total_pivot.append(_test_pivot_total(k)[1])

    plt.loglog(pivots, without_pivot, marker=next(markers), label="Sans pivot")
    plt.loglog(pivots, with_partial_pivot, marker=next(markers), label="Avec pivot partiel")
    #plt.loglog(pivots, with_total_pivot, marker=next(markers), label="Avec pivot total")
    plt.xlabel(" Valeur de k")
    plt.ylabel(" Erreur en solution")
    print("Error without pivot\n", without_pivot)
    print("Error with partial pivot\n", with_partial_pivot)
    #print("Error with total pivot\n", with_total_pivot)
    plt.legend()
    plt.show()


def func(k1=-4e6, k2=1., t=1.0e-9):
    return np.array([[k1, k2, k2],
                     [k2,  t, 0.],
                     [k2, 0.,0.]])

def test_scaling():
    """
    Petit exemple pour montrer l'apport du scaling
    sur la précision du calcul
    """
    A = func(k1=-1e9, k2=1e8, t=1.e-6)
    x_ex = np.array([1., 1., 1.])
    b = A.dot(x_ex)
    LU = lu_kji(A.copy())
    L = np.tril(LU)
    L[np.diag_indices(A.shape[0])] = 1.
    U = np.triu(LU)
    y = b.copy()
    forward_col(L,y)
    backward_col(U,y)
    print("Exact solution is :", x_ex)
    print("Without scaling, numerical solution is :", y)
    D1 = np.diag([.001,  1., 10.])
    D2 = np.diag([.001,  1., 10.])
    scaled_A = np.dot(np.dot(D1,A),D2)
    print("scaled(A) is :\n", scaled_A)
    scaled_rhs = np.dot(D1,b)
    #LU = lu_kji(scaled_A.copy())
    #L = np.tril(LU)
    #L[np.diag_indices(A.shape[0])] = 1.
    #U = np.triu(LU)
    #y = scaled_rhs.copy()
    #forward_col(L,y)
    #backward_col(U,y)
    y = la.solve(scaled_A, scaled_rhs)
    y = np.dot(D2,y)
    print("With scaling, numerical solution is :", y)

def test_iterative_refinement():
    """
    Petit exemple pour montrer l'apport du raffinement iteratif
    """
    #A = np.array([[1., -1.000000001],
    #              [-1.000000001,  1.]])

    #A = np.array([[-1.e9, 1.e8, 1.e8],
    #              [1.e8,1.e-6, 0.0],
    #              [1.e8,0.0, 1.0],
    #              ])
    A = _func_pivot_test(k=1.e-9)
    x_ex = np.array([1., 1., 1.])
    b = A.dot(x_ex)
    LU = lu_kji(A.copy())
    L = np.tril(LU)
    L[np.diag_indices(A.shape[0])] = 1.
    U = np.triu(LU)
    print("Exact solution is :", x_ex)

    def solve(b):
        x = b.copy()
        forward_col(L,x)
        backward_col(U,x)
        return x
    x = solve(b)
    print("Numerical solution is :", x)
    r = b - A.dot(x)
    errors = []
    errors.append(la.norm(x-x_ex))
    print("Residual is :", r)
    z = solve(r)
    x += z
    errors.append(la.norm(x-x_ex))
    print("Numerical solution is :", x)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(errors, marker=next(markers), label="Erreur sans pivot")
    ax.set_xlabel("Iteration")
    ax.set_ylabel("Erreur en solution")
    ax.legend()
    fig.savefig("raffinement_iteratif.pdf")
    plt.show()


if __name__ == "__main__":
    pivot_example()
    print("Iterative refinment test")
    test_iterative_refinement()
#    A = _func_pivot_test()
#     L, U, P = lu_kji_partial(A.copy())
#     print("A\n", A)
#     print("PA\n",np.dot(P,A))
#     print("LU\n",np.dot(L,U))
#     print("Err\n",np.dot(P,A) - np.dot(L,U))
#
#     A = _func_pivot_test()
#     L, U, P, Q = lu_kji_total(A.copy())
#     print("A\n", A)
#     print("PAQ\n", np.dot(np.dot(P,A),Q))
#     print("LU\n",np.dot(L,U))
#     print("Err\n", np.dot(np.dot(P,A),Q) - np.dot(L,U))


#    A = np.random.rand(6, 6)
#    A += (100. * np.diag(np.random.rand(6)) +10.)
#    L = np.tril(A)
#    U = np.triu(A)
#    b = np.arange(6, dtype=np.double)
#    x = forward_row(L, b)
#    print(b - np.dot(L, x))
#
#    x = forward_col(L, b.copy())
#    print(b - np.dot(L, x))
#
#    x = backward_col(U, b.copy())
#    print(b - np.dot(U, x))
#
#    for func in (lu_kji, lu_jki):
#        LU = func(A.copy())
#        LL = np.tril(LU, k=-1) + np.eye(LU.shape[0])
#        UU = np.triu(LU)
#        #print(A)
#        #print(np.dot(LL, UU))
#        print(A - np.dot(LL, UU))
#
#
#    B = A + A.transpose()
#    L = cholesky(B.copy())
#    #print(B)
#    #print(np.dot(L, L.transpose()))
#    print(B - np.dot(L, L.transpose()))
