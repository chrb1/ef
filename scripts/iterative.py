# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 20:08:52 2016

@author: christophe
"""
import numpy as np
import scipy as sp
import scipy.linalg as la
import scipy.sparse as spa
import scipy.sparse.linalg as sla
import pylab as plt

from itertools import cycle
markers = cycle("+xos")

_eps =1e-12
_maxiter=500

# TODO: Class hierarchy for iterative solvers
#class IterativeSolver(object):
#    """ Base class for all itrative solvers """
#    def __init__(self, *args, **kwargs):
#        self._name = None

def _basic_check(A, b, x0):
    """ Common check for clarity """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")
    if(b.size != n):
        raise ValueError("Bad rhs size")
    if (x0 is None):
        x0 = np.zeros(n)
    if(x0.size != n):
        raise ValueError("Bad initial value size")
    return x0

def Jacobi(A, b, x0=None, eps=_eps, maxiter=_maxiter):
    """
    Methode itérative stationnaire de Jacobi
    Convergence garantie si A est à diagonale dominante stricte
    A = D - E - F avec D diagonale, E (F) tri inf. (sup.) stricte
    Le préconditionneur est diagonal (M = D, N = E+F)
    """
    #TODO: Control overwrite_x0
    x = _basic_check(A, b, x0)
    M = sp.diag(A)
    if abs(M).min() < 1.0e-20:
        raise ArithmeticError("Jacobi will diverge")

    output = [1.]
    r = b - A.dot(x)
    nr0 = la.norm(r)
    err = nr0
    i=0
    while (err>eps and i<maxiter):
        x += r / M
        r = b - A.dot(x)
        err = la.norm(r)/nr0
        output.append(err)
        i+=1

    return x, output


def JOR(A, b, x0=None, omega=0.5, eps=_eps, maxiter=_maxiter):
    """
    Methode itérative stationnaire de sur-relaxation (Jacobi over relaxation)
    Convergence garantie si A est à diagonale dominante stricte
    A = D - E - F avec D diagonale, E (F) tri inf. (sup.) stricte
    Le préconditionneur est diagonal M = (1./omega) * D

    # TODO: Ajouter des bornes pour JOR ?
    Remarque: Si Jacobi converge alors JOR converge pour omega <= 1.
    """
    #TODO: Control overwrite_x0
    x = _basic_check(A, b, x0)
    M = sp.diag(A)
    if abs(M).min() < 1.0e-20:
        raise ArithmeticError("Jacobi will diverge")

    output = [1.]
    r = b - A.dot(x)
    nr0 = la.norm(r)
    err = nr0
    i=0
    while (err>eps and i<maxiter):
        x += omega * (r / M)
        r = b - A.dot(x)
        err = la.norm(r)/nr0
        output.append(err)
        i+=1

    return x, output


def Gauss_Seidel(A, b, x0=None, eps=_eps, maxiter=_maxiter):
    """
    Methode itérative stationnaire de Gauss-Seidel
    Convergence garantie si A est symétrique définie positive
    A = D - E - F avec D diagonale, E (F) tri inf. (sup.) stricte
    Le préconditionneur est tri. inf. M = D - E
    """
    x = _basic_check(A, b, x0)
    M = sp.tril(A) # Sparse to Dense conversion because, sla does not have
    # sparse triangular solve  in my version.

    output = [1.]
    r = b - A.dot(x)
    nr0 = la.norm(r)
    err = nr0
    i=0
    while (err>eps and i<maxiter):
        x += la.solve_triangular(M, r, lower=True)
        r = b - A.dot(x)
        err = la.norm(r)/nr0
        output.append(err)
        i+=1
    return x, output


def SOR(A, b, x0=None, omega=1.5, eps=_eps, maxiter=_maxiter):
    """
    Methode itérative stationnaire de sur-relaxation successive
    (Sucessive Over Relaxation)

    A = D - E - F avec D diagonale, E (F) tri inf. (sup.) stricte
    Le préconditionneur est tri. inf. M = (1./omega) * D - E

    * Divergence garantie pour omega <= 0. ou omega >= 2.0
    * Convergence garantie si A est symétrique définie positive pour
    0 < omega  < 2.
    * Convergence garantie si A est à diagonale dominante stricte pour
    0 < omega  <= 1.

    """
    if (omega > 2.) or (omega < 0.):
        raise ArithmeticError("SOR will diverge")

    n, m = A.shape
    x = _basic_check(A, b, x0)
    M = sp.tril(A) # Sparse to Dense conversion because, sla does not have
    # sparse triangular solve  in my version.
    M[np.diag_indices(n)] /= omega


    output = [1.]
    r = b - A.dot(x)
    nr0 = la.norm(r)
    err = nr0
    i=0
    while (err > eps and i < maxiter):
        x += la.solve_triangular(M, r, lower=True)
        r = b - A.dot(x)
        err = la.norm(r)/nr0
        output.append(err)
        i+=1
    return x, output


def SSOR(A, b, x0=None, omega=1.5, eps=_eps, maxiter=_maxiter):
    """
    Methode itérative stationnaire de sur-relaxation successive symétrique
    (Symmetric Sucessive Over Relaxation)
    """
    raise NotImplementedError("SSOR")

def OptimalGradient(A, b, x0=None, eps=_eps, maxiter=_maxiter,
                    precond=None):
    """ """
    n, m = A.shape
    x = _basic_check(A, b, x0)
    # M = sp.tril(A) # Sparse to Dense conversion because, sla does not have
    # sparse triangular solve  in my version.

    output = [1.]
    r = b - A.dot(x)
    nr0 = la.norm(r)
    err = nr0
    i=0

    if precond is None:
        while (err>eps and i<maxiter):
            q = A.dot(r)
            alpha = np.inner(r,r) / np.inner(r, q)
            x += alpha * r
            r -= alpha * q
            err = la.norm(r)/nr0
            output.append(err)
            i+=1
    else:
        z = r.copy()
        while (err>eps and i<maxiter):
            z = precond(r, z)
            rho = np.inner(r,z)
            q = A.dot(z)
            alpha = rho / np.inner(z, q)
            x += alpha * z
            r -= alpha * q
            err = la.norm(r)/nr0
            output.append(err)
            i+=1

    return x, output


def ConjugateGradient(A, b, x0=None, eps=_eps, maxiter=_maxiter,
                      precond=None):
    """ """
    n, m = A.shape
    x = _basic_check(A, b, x0)
    # M = sp.tril(A) # Sparse to Dense conversion because, sla does not have
    # sparse triangular solve  in my version.

    output = [1.]
    r = b - A.dot(x)
    p = r.copy()
    nr0 = la.norm(r)
    err = nr0
    i=0
    if precond is None:
        while (err>eps and i<maxiter):
            q = A.dot(p)
            alpha = np.inner(p,r) / np.inner(p, q)
            x += alpha * p
            r -= alpha * q
            beta = np.inner(q,r) / np.inner(p,q)
            p = r - beta*p
            err = la.norm(r)/nr0
            output.append(err)
            i+=1
    else:
        z = r.copy()
        z = precond(r, z)
        p = z.copy()
        while (err>eps and i<maxiter):
            rho = np.inner(r,z)
            q = A.dot(p)
            alpha = rho / np.inner(p, q)
            x += alpha * p
            r -= alpha * q
            z = precond(r, z)
            beta = np.inner(z,r) / rho
            p = z + beta*p
            err = la.norm(r)/nr0
            output.append(err)
            i+=1
    return x, output



def example_generator(n=100, k=1e3, off=1.):
    """
    Generate an example and plot the results

    """
    L = np.random.rand((n*n))
    L.shape = n, n
    A = - (L + L.transpose())
    A += k * np.diag(np.random.rand(n) + off)
    b = np.random.rand((n))

    P = A.diagonal()
    def prec(r, z):
        z[:] = r/P
        return z
    #x0 = np.random.rand((n))
    x0 = np.zeros((n))
    x_ex = la.solve(A,b)
    print("x_exact")
    print(x_ex)

    solvers = [
            Jacobi(A,b,x0.copy()),
            Gauss_Seidel(A,b,x0.copy()),
            JOR(A,b,x0.copy(), omega=1.2),
            SOR(A,b,x0.copy(), omega=.5),
            OptimalGradient(A,b,x0.copy()),
            ConjugateGradient(A,b,x0.copy()),
            OptimalGradient(A,b,x0.copy(), precond=prec),
            ConjugateGradient(A,b,x0.copy(), precond=prec)
            ]
    labels = [
            "Jacobi",
            "Gauss Seidel",
            "JOR, omega=%s"%1.2,
            "SOR, omega=%s"%.5,
            "Optimal Gradient",
            "Conjugate Gradient",
            "Optimal Gradient (P=diag)",
            "Conjugate Gradient(P=diag)"
            ]


    #plt.style.use("live")

    for (xsol, res),lab in zip(solvers, labels):
        plt.semilogy(res, label=lab, marker=next(markers), markevery=int(len(res)/10+1))

    plt.xlabel("Iteration")
    plt.ylabel("l2-norm of residual")
    plt.legend(loc="best")
    plt.show()
    return A,b

def example_from_file(fname):
    """
    Read operator and rhs and plot the results
    The convention is :
    A and b were stored in fname

    """
    data = np.load(fname)
    A = data["A"]
    b = data["b"]
    n,m = A.shape

    P = A.diagonal()
    def prec(r, z):
        z[:] = r/P
        return z
    x0 = np.zeros((n))
    x_ex = la.solve(A,b)

    solvers = [
            Jacobi(A,b,x0.copy()),
            Gauss_Seidel(A,b,x0.copy()),
            JOR(A,b,x0.copy(), omega=1.2),
            SOR(A,b,x0.copy(), omega=.5),
            OptimalGradient(A,b,x0.copy()),
            ConjugateGradient(A,b,x0.copy()),
            OptimalGradient(A,b,x0.copy(), precond=prec),
            ConjugateGradient(A,b,x0.copy(), precond=prec)
            ]
    labels = [
            "Jacobi",
            "Gauss Seidel",
            "JOR, omega=%s"%1.2,
            "SOR, omega=%s"%.5,
            "Optimal Gradient",
            "Conjugate Gradient",
            "Optimal Gradient (P=diag)",
            "Conjugate Gradient(P=diag)"
            ]


    #plt.style.use("live")
    w,h = plt.figaspect(0.5)
    fig = plt.figure(figsize=(w,h))
    ax = fig.add_subplot(111)

    for (xsol, res),lab in zip(solvers, labels):
        ax.semilogy(res, label=lab, marker=next(markers), markevery=int(len(res)/10+1))

    ax.set_xlabel("Iteration")
    ax.set_ylabel("l2-norm of residual")
    ax.legend(loc="best")
    fig.tight_layout()
    fig.savefig("comparaison_iterative_solver.pdf")
    plt.show()
    return A,b

#CG_X = list()
#
#def report(x):
#    CG_X.append(x.copy())
#    #print("callback")
#    #print(x)
#
#
#def CG(A, b, x0=None, rtol=_eps, precond=None):
#    nr0 = la.norm(b-np.dot(A, x0))
#    x, info = sla.cg(A,b,x0,tol=rtol, M=precond, maxiter=100, callback=report)
#    #print(info)
#    #nr0 = la.norm(b-np.dot(A,CG_X[0]))
#    out_res = [1.] + [ (la.norm(b - np.dot(A,xi))/nr0) for xi in CG_X]
#
#    return x, out_res

if __name__ == "__main__":
    # TODO: figer un exemple représentatif
    example_from_file("ex1.npz")




