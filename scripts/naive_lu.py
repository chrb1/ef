# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 09:27:58 2016

@author: christophe
"""

import numpy as np
import scipy.sparse as spa
import scipy.sparse.linalg as sla
import scipy.linalg as la
import pylab as plt

k = 1e-20


def err_naive_lu(k):

    A = np.array([[k,1.],
                  [1.   ,1.]])

    L = np.array([[1., 0.],
                  [(1./k),1.]])

    U = np.array([[k, 1.],
                  [0.,1. - (1./k)]])


    E = A - np.dot(L,U)

    err = la.norm(E)
    err_rel = err / la.norm(A)
    print("Naive LU : (err,rel) = (%s,%s)"%(err,err_rel))
    return err_rel

def less_err_naive_lu(k):
    """ A -> PA """
    A = np.array([[1.   ,1.],
                  [k,1.]])

    L = np.array([[1., 0.],
                  [(k),1.]])

    U = np.array([[1., 1.],
                  [0.,1. - k]])


    E = A - np.dot(L,U)

    err = la.norm(E)
    err_rel = err / la.norm(A)
    print("Less Naive LU(err,rel) = (%s,%s)"%(err,err_rel))
    return err_rel


if __name__ == "__main__":

    k = np.logspace(1,-20,num=51)
    naive = [err_naive_lu(ik) for ik in k ]
    pivot = [less_err_naive_lu(ik) for ik in k ]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_ylim(-.1, 1.)
    ax.semilogx((1./k),naive, label="Sans pivot")
    ax.semilogx((1./k),pivot, label="Avec pivot partiel")

    ax.set_xlabel(" 1/k ")
    ax.set_ylabel(" | A - LU |/|A|")
    ax.legend(loc="upper left")

    plt.show()
